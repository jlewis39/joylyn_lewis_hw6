package com.cs441.hw6

//CS 441 HW6   : Implementing calculator functionality as a lambda function
//Submitted by : Joylyn Lewis

//Import the required InputSteam and OutputStream libraries for using the byte stream interface
import java.io.{InputStream, OutputStream}
import com.typesafe.config._
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger

//Define a scala class 'InputData' and use the byte stream interface to deserialize the input passed to the Lambda function as this scala class
//Here, number1 and number2 are the two operands and opCode is the mathematical operation that can be performed on the two operands like +, -, * and /
case class InputData(number1: String, number2: String, opCode: String)

object CS441HW6_Lambda{

  //Load the application's configuration from the classpath resource basename
  val config = ConfigFactory.load("application.conf")

  //Create a logger instance for supplying logging statements
  val logger = Logger(LoggerFactory.getLogger(config.getString("Lambda.logger")))

  val scalaMapper = {
    //Jackson ObjectMapper class helps to serialize objects into JSON and deserialize JSON string into objects
    import com.fasterxml.jackson.databind.ObjectMapper
    import com.fasterxml.jackson.module.scala.DefaultScalaModule
    new ObjectMapper().registerModule(new DefaultScalaModule)
  }

  def handler(input: InputStream, output: OutputStream) : Unit ={

    //Read the input to the lambda function as of type InputData which consists of the parameters - number1, number2 and opCode
    val operation = scalaMapper.readValue(input, classOf[InputData])

    var result:String = ""
    //Output error message if any of the inputs are not properly specified
    if(operation.number1 == null || operation.number2 == null || operation.opCode == null){
      result = config.getString("Lambda.invalidInput")
      logger.error("Error in submitting input to the Lambda function")
      output.write(result.getBytes(config.getString("Lambda.charsetName")))
    }

    //Perform desired operation based on the opcode passed as input to the lambda function
    operation.opCode match {

      case "+" => result = (operation.number1.toDouble + operation.number2.toDouble).toString
      case "-" => result = (operation.number1.toDouble - operation.number2.toDouble).toString
      case "*" => result = (operation.number1.toDouble * operation.number2.toDouble).toString
      case "/" => result = (operation.number1.toDouble / operation.number2.toDouble).toString
      case _   => {logger.error("Error in submitting input to the Lambda function")
                    result = "400 Invalid Input"}

    }
    //Write the result of the operation perform
    output.write(result.getBytes("UTF-8"))

  }

}
