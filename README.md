###CS 441 HW6: Implementing calculator functionality as a lambda function
###Submitted By: Joylyn Lewis 

**Important files present in the repository:**  
- **/src/main/resources/application.conf** contains the configuration settings for running the program  
- **/src/main/scala/com/cs441/hw6/CS441HW6_Lambda.scala** contains the lambda function implementation  
- **Screenshots of results** contains results of executing the Lambda function, results of testing the Post Method with a JSON payload via the AWS API Gateway to invoke the Lambda function 
and results of testing the Post Method with a JSON payload via Postman using the Invoke URL of the deployed API

**Steps to compile the code:**  
- Go to the project directory in command line  
- Enter command 'sbt clean compile assembly' to create the assembly jar file

**Steps to set up the Lambda function on AWS**  
- In the IAM Console, first create an API Gateway assumable IAM role. Create a new policy that allows invoking the lambda function.  
- The role 'lambda_invoke_function_assume_apigw_role' was created and trust relationships to include apigateway along with lambda service was done. This generated the role ARN: arn:aws:iam::313392046371:role/lambda_invoke_function_assume_apigw_role  
- Next, the lambda function 'lambdaCalculator' was created by choosing Create function option in the Lambda console.  
- The assembly jar created was uploaded here and the role ARN cretaed in IAM was provided as the execution role.  
- The lambda function was tested by defining test events for add, subtract, multiply and divide operations. The input is provided in JSon format with fields number1 and number denoting the two operands and opCode denoting the operation to be performed.  
- After the lambda function was tested, an API 'LambdaCalc'was created with a POST method with a JSON payload to call the Lambda function 'lambdaCalculator'  
- The Method Execution of the API was tested by providing JSON input in the Request body and checking that the result is correct.  
- Once the Method Execution was tested, the API was deployed to the Dev stage. The Invoke URL is 'https://xcvder31qb.execute-api.us-east-1.amazonaws.com/Dev/calc'  
- Using the Invoke URL, messages were sent and tested in Postman by providing the JSON input payload as part of the Body to the POST method. The results returned in Postman were verified for each of the operations.  

**To test the invocation of Lambda function via REST:**  
- In Postman, for the message type 'Post', provide url **https://xcvder31qb.execute-api.us-east-1.amazonaws.com/Dev/calc**  
- Select the Body to provide input as below:  
{ 
  "number1": "10",  
  "number2": "2",  
  "opCode": "+"  
}  
  
The output can be seen in the response body.   

The opCode can be changed to '-', '*', '/' to test the other operations.

Results obtained of the above three - Lambda function, POST method of AWS API Gateway invoking the Lambda function and posting messages via the deployed API URL in Postman is available in the document 'Screenshots of results'

**Calculator Lambda function Implementation:**  
- The lambda function is written in Scala using the byte stream interface and the Jackson Scala module that enables deserializing input passed to the lambda function.  
- A scala class InputData with paramerters number1, number2 and opCode is defined to take in the values provided as input to the lambda function in the form of a Json payload.  
- The handler function is defined to return the result of the operation performed on the operands.

NOTE: This submission attempts only the implementation of Lambda function and invoking it via the AWS API Gateway. The gRPC part of the homework could not be attempted within the extended deadline timeframe. 
